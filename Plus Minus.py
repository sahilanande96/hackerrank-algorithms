#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the plusMinus function below.
def plusMinus(arr):
    size = len(arr)
    pos = 0
    neg =0
    zero = 0
    for _ in arr:
        if _ > 0:
            pos+=1
        elif _ < 0:
            neg+=1
        else:
            zero+=1
    print("{:.6f}".format(pos/size))
    print("{:.6f}".format(neg/size))
    print("{:.6f}".format(zero/size))

if __name__ == '__main__':
    n = int(input())

    arr = list(map(int, input().rstrip().split()))

    plusMinus(arr)

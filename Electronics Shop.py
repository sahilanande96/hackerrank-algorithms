#!/bin/python3

import os
import sys

#
# Complete the getMoneySpent function below.
#
def getMoneySpent(keyboards, drives, b):
    #
    # Write your code here.
    #
    keyboards_count = len(keyboards)
    drives_count = len(drives)
    keyboards.sort()
    drives.sort()
    result = []
    
    
    if ((keyboards[0] >= b) or (drives[0] >= b)):
        return(-1)
    
    if ((keyboards[0]+drives[0]) > b):
        return(-1)
    
    for _ in range(keyboards_count):
        for j in range(drives_count):
            if (keyboards[_] + drives[j]) <= b:
                result.append((keyboards[_] + drives[j]))
    
    if not result:
        return(-1)
    else:
        return(max(result))

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    bnm = input().split()

    b = int(bnm[0])

    n = int(bnm[1])

    m = int(bnm[2])

    keyboards = list(map(int, input().rstrip().split()))

    drives = list(map(int, input().rstrip().split()))

    #
    # The maximum amount of money she can spend on a keyboard and USB drive, or -1 if she can't purchase both items
    #

    moneySpent = getMoneySpent(keyboards, drives, b)

    fptr.write(str(moneySpent) + '\n')

    fptr.close()

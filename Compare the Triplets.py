#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the compareTriplets function below.
def compareTriplets(a, b):
    count = 3
    eq = 0
    result = [0, 0] 
    for _ in range(count):
        if a[_]>b[_]:
            result[0]+=1
        elif a[_]<b[_]:
            result[1]+=1
        else:
            eq+=1
    return result

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    a = list(map(int, input().rstrip().split()))

    b = list(map(int, input().rstrip().split()))

    result = compareTriplets(a, b)

    fptr.write(' '.join(map(str, result)))
    fptr.write('\n')

    fptr.close()

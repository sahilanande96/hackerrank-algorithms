#!/bin/python3

import os
import sys

#
# Complete the pageCount function below.
#
def pageCount(n, p):
    #
    # Write your code here.
    #
    from_front = p // 2
    from_end = (n - p)//2
    # print(from_front)
    # print(from_end)
    if (from_front < from_end):
        return(from_front)
    elif(from_end < from_front):
        return(from_end)
    else:
        return(from_front)

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input())

    p = int(input())

    result = pageCount(n, p)

    fptr.write(str(result) + '\n')

    fptr.close()

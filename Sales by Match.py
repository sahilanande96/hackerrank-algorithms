#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the sockMerchant function below.
def sockMerchant(n, ar):
    temp = 0
    result = 0
    unique_number = list(set(ar))
    # print(unique_number)
    for _ in unique_number:
        occurence = ar.count(_)
        # print(occurence)
        temp = occurence//2
        result += temp
    return result
        

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input())

    ar = list(map(int, input().rstrip().split()))

    result = sockMerchant(n, ar)

    fptr.write(str(result) + '\n')

    fptr.close()

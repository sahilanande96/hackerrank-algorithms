#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the miniMaxSum function below.
def miniMaxSum(arr):
    asc = 0
    dec = 0
    arr.sort()
    for _ in arr[:-1]:
        asc += _ 
    
        dec = asc + arr[-1] -arr[0]
        
    print(str(asc)+ " " + str(dec))
if __name__ == '__main__':
    arr = list(map(int, input().rstrip().split()))

    miniMaxSum(arr)

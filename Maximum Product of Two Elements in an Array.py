"""
Given the array of integers nums, you will choose two different indices i and j of that array. Return the maximum value of (nums[i]-1)*(nums[j]-1).
 

Example 1:

Input: nums = [3,4,5,2]
Output: 12 
Explanation: If you choose the indices i=1 and j=2 (indexed from 0), you will get the maximum value, that is, (nums[1]-1)*(nums[2]-1) = (4-1)*(5-1) = 3*4 = 12. 
Example 2:

Input: nums = [1,5,4,5]
Output: 16
Explanation: Choosing the indices i=1 and j=3 (indexed from 0), you will get the maximum value of (5-1)*(5-1) = 16.
Example 3:

Input: nums = [3,7]
Output: 12
 

Constraints:

2 <= nums.length <= 500
1 <= nums[i] <= 10^3
"""

# Code
class Solution:
    def maxProduct(self, nums: List[int]) -> int:
        nums.sort()
        return((nums[-1]-1) * (nums[-2]-1))
        # for _ in range(2):
        #     # print("Loop ", _)
        #     for idx in range(len(nums)-1):
        #         # print("idx ", idx)
        #         if nums[idx] > nums[idx+1]:
        #             temp = nums[idx]
        #             nums[idx] = nums[idx+1]
        #             nums[idx+ 1] = temp
        # # print(nums)
        # return((nums[-1]-1) * (nums[-2]-1))
# I had thought traversing the array just twice to get the 2 max numbers will be faster than sorting the whole array, but apparently not!

"""
104 / 104 test cases passed.
Status: Accepted
Runtime: 40 ms
Memory Usage: 14.4 MB
"""

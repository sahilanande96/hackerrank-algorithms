#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the breakingRecords function below.
def breakingRecords(scores):
    size = len(scores)
    max_score = scores[0]
    min_score = scores[0]
    count_max = 0
    count_min = 0
    for _ in scores:
        if max_score < _:
            max_score = _
            # print(max_score)
            count_max += 1
        if min_score > _ :
            min_score = _
            count_min += 1
            # print(min_score)
            
    res = [count_max, count_min]
    return(res)

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input())

    scores = list(map(int, input().rstrip().split()))

    result = breakingRecords(scores)

    fptr.write(' '.join(map(str, result)))
    fptr.write('\n')

    fptr.close()
